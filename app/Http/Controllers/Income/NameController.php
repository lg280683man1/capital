<?php

namespace App\Http\Controllers\Income;

use App\Http\Controllers\Controller;
use App\Http\Requests\IncomeNameStoreRequest;
use App\Models\IncomesName;

class NameController extends Controller
{
    public function create()
    {
        return view('forms.incomes-name-create');
    }

    public function store(IncomeNameStoreRequest $request)
    {
        IncomesName::create([
            'name' => $request->input('name'),
        ]);

        return redirect()->route('main');
    }
}
