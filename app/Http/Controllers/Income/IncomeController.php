<?php

namespace App\Http\Controllers\Income;

use App\Http\Controllers\Controller;
use App\Http\Requests\IncomeStoreRequest;
use App\Models\Income;

class IncomeController extends Controller
{
    public function store(IncomeStoreRequest $request)
    {
        Income::create([
            'incomes_names_id' => $request->input('incomes_names_id'),
            'total' => $request->input('total'),
        ]);

        return redirect()->route('main');
        /**
         * TODO добавить всплывающее сообщение об успешном добавлении данных
         */
    }
}
