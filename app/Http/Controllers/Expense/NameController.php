<?php

namespace App\Http\Controllers\Expense;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpenseNameStoreRequest;
use App\Models\ExpensesName;

class NameController extends Controller
{
    public function create()
    {
        return view('forms.expenses-name-create');
    }

    public function store(ExpenseNameStoreRequest $request)
    {
        ExpensesName::create([
            'name' => $request->input('name'),
        ]);

        return redirect()->route('main');
    }
}
