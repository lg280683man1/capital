<?php

namespace App\Http\Controllers\Expense;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpenseStoreRequest;
use App\Models\Expense;

class ExpenseController extends Controller
{
    public function store(ExpenseStoreRequest $request)
    {
        Expense::create([
            'expenses_names_id' => $request->input('expenses_names_id'),
            'total' => $request->input('total')
        ]);

        return redirect()->route('main');
        /**
         * TODO добавить всплывающее сообщение об успешном добавлении данных
         */
    }
}
