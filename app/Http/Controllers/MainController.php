<?php

namespace App\Http\Controllers;

use App\Models\ExpensesName;
use App\Models\IncomesName;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $incomesNames = IncomesName::all();
        $expensesNames = ExpensesName::all();

        return view('index', [
            'incomesNames' => $incomesNames,
            'expensesNames' => $expensesNames,
        ]);


    }
}
