<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Приложение учета доходов и расходов</title>
</head>
<body>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('logout') }}" method="post">
    @csrf
    <input type="submit" value="Разлогиниться">
</form>

<h1>Capital - приложение учета доходов и расходов</h1>

<h3>Блок доходов</h3>

    @if($incomesNames->isEmpty())
        <p>Вы не создали ни одной статьи доходов</p>
    @else
        <form action="{{ route('income.store') }}" method="post">
            @csrf
            <select name="incomes_names_id">
                <option disabled selected value> -- выбери статью дохода -- </option>
                @foreach($incomesNames as $name)
                    <option value="{{ $name->id }}">{{ $name->name }}</option>
                @endforeach
            </select><br><br>
            <input type="number" step="0.01" name="total" placeholder="0.00"> рублей<br><br>
            <input type="submit" value="Добавить">
        </form>
    @endif

<p>Если в списке нет необходимой статьи доходов, то ее нужной <a href="{{ route('income-name.create') }}" target="_blank">Добвить</a></p>
<br><br>

<h3>Блок расходов</h3>

    @if($expensesNames->isEmpty())
        <p>Вы не создали ни одной статьи доходов</p>
    @else
        <form action="{{ route('expense.store') }}" method="post">
            @csrf
            <select name="expenses_names_id">
                <option disabled selected value> -- выбери статью расхода -- </option>
                @foreach($expensesNames as $name)
                    <option value="{{ $name->id }}">{{ $name->name }}</option>
                @endforeach
            </select><br><br>
            <input type="number" step="0.01" name="total" placeholder="0.00"> рублей<br><br>
            <input type="submit" value="Добавить">
        </form>
    @endif


<p>Если в списке нет необходимой статьи расходов, то ее нужной <a href="{{ route('expense-name.create') }}" target="_blank">Добавить</a></p>

<p><a href="">Переход на страницу вывода статистики</a></p>


</body>
</html>
