<?php

use App\Http\Controllers\Expense\ExpenseController;
use App\Http\Controllers\Income\IncomeController;
use App\Http\Controllers\Income\NameController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/', [MainController::class, 'index'])->name('main');

    Route::post('/income', [IncomeController::class, 'store'])->name('income.store');

    Route::get('/income-name/create', [NameController::class, 'create'])->name('income-name.create');
    Route::post('/income-name/create', [NameController::class, 'store'])->name('income-name.store');

    Route::post('/expense', [ExpenseController::class, 'store'])->name('expense.store');

    Route::get('/expense-name/create', [\App\Http\Controllers\Expense\NameController::class, 'create'])->name('expense-name.create');
    Route::post('/expense-name/create', [\App\Http\Controllers\Expense\NameController::class, 'store'])->name('expense-name.store');

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
